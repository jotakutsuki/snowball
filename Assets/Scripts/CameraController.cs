﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject ball;
    public AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        //audio.Play();
    }

    void Update()
    {
        Camera.main.transform.LookAt(new Vector3(0, ball.transform.position.y, ball.transform.position.z + 2));
        if (ball.GetComponent<BallController>().hasArrivedTop)
        {
            //mode where landing
        }else if (ball.GetComponent<BallController>().hasArrived)
        {
            //mode destroy
            transform.position = new Vector3(0, 5, ball.transform.position.z - 5);
        }else if (!ball.GetComponent<BallController>().hasLunched)
        {
            //lunchmode
        }
        else
        {
            //standar mode
            transform.position = new Vector3(0, 5, ball.transform.position.z - 5);
        }
       
        //transform.rotation = Quaternion.Euler(new Vector3(55, 0, 0));
    }
}
