﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BlocksBuilder : MonoBehaviour
{

    public GameObject build;
    public int numberOfBuildings;
    //public float distance;
    public float density;
    public float mass;
    public List<GameObject> buildings;
    public int collitions = 0;
    public List<GameObject> downBuildings;
    public TextMeshPro crashesUi;

    void Start()
    {
        downBuildings = new List<GameObject>();
        buildings = new List<GameObject>();
        int i = 0;
        while (buildings.Count < numberOfBuildings)
        {
            if (Random.value > 0.2f)
            {
                Vector3 scale = new Vector3(Random.Range(.1f, .5f), Random.Range(.8f, 1.2f), Random.Range(.1f, .5f));
                Vector3 position = new Vector3(Random.Range(-3, 3), scale.y/2, 45 + i / density);
                Quaternion rotation = Quaternion.Euler(0, Random.Range(0, 30), 0);

                Collider[] allOverlappingColliders = Physics.OverlapBox(position, scale / 2, rotation);

                bool colpasing = false;
                foreach (Collider collider in allOverlappingColliders)
                {
                    if ("Build".Equals(collider.gameObject.tag))
                        colpasing = true;
                }

                if (!colpasing)
                {
                    GameObject buiding = Instantiate(build, position, rotation, transform);
                    buiding.transform.localScale = scale;
                    buiding.GetComponent<Rigidbody>().mass = mass;
                    buildings.Add(buiding);
                }
            }
            i++;
        }
    }

    public void TryInstanciate(GameObject obj, Vector3 position)
    {
        if (SpaceAvailable(position, obj.GetComponent<BoxCollider>()))
        {
            //Instanciate(obj, position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if ("CornerAux".Equals(other.gameObject.tag) && !downBuildings.Contains(other.gameObject))
        {
            downBuildings.Add(other.gameObject);
            crashesUi.SetText(downBuildings.Count.ToString());
        }

        //if ("CornerAux".Equals(other.gameObject.tag))
        //{
        //    //collitions++;
        //    Collider[] allOverlappingColliders = Physics.OverlapBox(transform.position, GetComponent<BoxCollider>().size/2);
        //    foreach (Collider collider in allOverlappingColliders)
        //    {
        //        if ("CornerAux".Equals(other.gameObject.tag))
        //            Debug.Log(collider.gameObject.name);
        //    }
        //}
    }

    public bool SpaceAvailable(Vector3 position, Collider col)
    {
        Vector3 colliderSize = new Vector3(col.bounds.extents.x, col.bounds.extents.y, col.bounds.extents.z);
        bool fits = true;
        return fits;
    }
}
