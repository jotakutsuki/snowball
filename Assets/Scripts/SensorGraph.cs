﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorGraph : MonoBehaviour
{
    Gyroscope m_Gyro;
    public GameObject particle;
    public GameObject particles;

    public float scaley;
    public float distance;
    public float speed;
    public int number;

    List<GameObject> attitudeXs;
    List<GameObject> attitudeYs;
    List<GameObject> attitudeZs;

    List<GameObject> rotationXs;
    List<GameObject> rotationYs;
    List<GameObject> rotationZs;

    void Start()
    {
        attitudeXs = new List<GameObject>();
        attitudeYs = new List<GameObject>();
        attitudeZs = new List<GameObject>();

        rotationXs = new List<GameObject>();
        rotationYs = new List<GameObject>();
        rotationZs = new List<GameObject>();

        m_Gyro = Input.gyro;
        m_Gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < rotationXs.Count; i++)
        {
            //if (i > number)
            //{
                
            //    GameObject.Destroy(attitudeXs[i]);
            //    attitudeXs.RemoveAt(i);
            //    GameObject.Destroy(attitudeYs[i]);
            //    attitudeYs.RemoveAt(i);
            //    GameObject.Destroy(attitudeZs[i]);
            //    attitudeZs.RemoveAt(i);

            //    GameObject.Destroy(rotationXs[i]);
            //    rotationXs.RemoveAt(i);
            //    GameObject.Destroy(rotationYs[i]);
            //    rotationYs.RemoveAt(i);
            //    GameObject.Destroy(rotationZs[i]);
            //    rotationZs.RemoveAt(i);
            //}

            //attitudeXs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);
            //attitudeYs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);
            //attitudeZs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);

            rotationXs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);
            rotationYs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);
            rotationZs[i].transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        //GameObject attitudeX = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.attitude.x / scaley + distance, 0), new Quaternion(), particles.transform);
        //attitudeX.GetComponent<SpriteRenderer>().color = Color.red;
        //GameObject attitudeY = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.attitude.y / scaley + distance, 0), new Quaternion(), particles.transform);
        //attitudeY.GetComponent<SpriteRenderer>().color = Color.green;
        //GameObject attitudeZ = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.attitude.z / scaley + distance, 0), new Quaternion(), particles.transform);
        //attitudeZ.GetComponent<SpriteRenderer>().color = Color.blue;

        GameObject rotationX = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.rotationRate.x / scaley - distance, 0), new Quaternion(), particles.transform);
        rotationX.GetComponent<SpriteRenderer>().color = Color.cyan;
        rotationX.GetComponent<SpriteRenderer>().enabled = true;
        GameObject rotationY = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.rotationRate.y / scaley - distance, 0), new Quaternion(), particles.transform);
        rotationY.GetComponent<SpriteRenderer>().color = Color.yellow;
        rotationY.GetComponent<SpriteRenderer>().enabled = true;
        GameObject rotationZ = GameObject.Instantiate(particle, transform.position + new Vector3(0, m_Gyro.rotationRate.z / scaley - distance, 0), new Quaternion(), particles.transform);
        rotationZ.GetComponent<SpriteRenderer>().color = Color.magenta;
        rotationZ.GetComponent<SpriteRenderer>().enabled = true;

        //attitudeXs.Add(attitudeX);
        //attitudeYs.Add(attitudeY);
        //attitudeZs.Add(attitudeZ);

        rotationXs.Add(rotationX);
        rotationYs.Add(rotationY);
        rotationZs.Add(rotationZ);
    }
}
