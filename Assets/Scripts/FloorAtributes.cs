﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorAtributes : MonoBehaviour
{
    public GameObject rock; 
    public GameObject genericWall;
    public int wallCuts;
    float randomFloorFactor;
    GameObject previewObjRef;
    List<GameObject> leftwall = new List<GameObject>();
    List<Vector3[]>  leftVertices = new List<Vector3[]>();
    List<Mesh> leftMesh = new List<Mesh>();
    List<MeshCollider> leftMeshCollider = new List<MeshCollider>();

    List<GameObject> rightwall = new List<GameObject>();
    List<Vector3[]> rightVertices = new List<Vector3[]>();
    List<Mesh> rightMesh = new List<Mesh>();
    List<MeshCollider> rightMeshCollider = new List<MeshCollider>();
    float scale;
    public TextMesh text;
    public GameObject[] christmasTree;

    public void setPrevObjRef(GameObject previewObjRef){
        this.previewObjRef = previewObjRef;
    }
    public List<Vector3[]> getLeftVert(){
        return this.leftVertices;
    }
    public List<Vector3[]> getRightVert()
    {
        return this.rightVertices;
    }


    void Start()
    {
        scale = transform.localScale.x;

        float size = transform.localScale.z / wallCuts;

        for (int i = 0; i<wallCuts; i++)
        {
            leftwall.Add(GameObject.Instantiate(genericWall,
                transform.position + new Vector3(-scale/2, 0, scale * (-.5f + size/2 + size * i)), 
                new Quaternion(), transform));
            leftwall[i].transform.localScale = new Vector3(.1f, size, .1f) ;
            leftMesh.Add(leftwall[i].GetComponent<MeshFilter>().mesh);
            leftVertices.Add(leftMesh[i].vertices);
            leftMeshCollider.Add(leftwall[i].GetComponent<MeshCollider>());

            rightwall.Add(GameObject.Instantiate(genericWall,
                transform.position + new Vector3(scale/2, 0, scale * (-.5f + size / 2 + size * i)),
                new Quaternion(), transform));
            rightwall[i].transform.localScale = new Vector3(.1f, size, .1f);
            rightMesh.Add(rightwall[i].GetComponent<MeshFilter>().mesh);
            rightVertices.Add(rightMesh[i].vertices);
            rightMeshCollider.Add(rightwall[i].GetComponent<MeshCollider>());
        }
    }

    public void Renew(){
        transform.Find("Text").gameObject.SetActive(false);
        
        rock.transform.localPosition = new Vector3(Random.Range(-0.5f,0.5f),0,0);
        randomFloorFactor = transform.parent.GetComponent<FloorController>().randomFactor;
        float roadWidth = transform.parent.GetComponent<FloorController>().roadWidth;
        //for (int i = 0; i < leftVertices[0].Length; i++)
        //{
        //    text.text = i.ToString();
        //    GameObject.Instantiate(text, leftVertices[0][i], new Quaternion());
        //    Debug.Log(i + ": " + leftVertices[0][i]);
        //}
        //0,13,23; inferior frontal izq
        //6,12,20; inferior traseo izq
        //2,8,22; superior frontal izq
        //4,10,21; superior trasero izq
        //1,14,16; inferior frontal derecho
        //3,9,17; superior frontal derecho
        //5,11,18; superior trasero derecho
        //7,15,19; inferior trasero derecho

        /*LeftWall Random movment*/
        List<Vector3[]> prevVert = previewObjRef.GetComponent<FloorAtributes>().getLeftVert();
        List<Vector3[]> prevVertRight = previewObjRef.GetComponent<FloorAtributes>().getRightVert();
        int prevSize = prevVert.Count;
        for (int i = 0; i < leftVertices.Count; i++)
        {
            float addvalue = Random.Range(-5, 5f) * randomFloorFactor;
            if (prevSize > 0)
            {
                leftVertices[i][0] = new Vector3(i == 0 ? prevVert[prevSize - 1][4].x : leftVertices[i - 1][4].x, leftVertices[i][0].y, leftVertices[i][0].z);
                leftVertices[i][13] = new Vector3(i == 0 ? prevVert[prevSize - 1][4].x : leftVertices[i - 1][4].x, leftVertices[i][0].y, leftVertices[i][0].z);
                leftVertices[i][23] = new Vector3(i == 0 ? prevVert[prevSize - 1][4].x : leftVertices[i - 1][4].x, leftVertices[i][0].y, leftVertices[i][0].z);

                leftVertices[i][6] = new Vector3(i == 0 ? prevVert[prevSize - 1][2].x : leftVertices[i - 1][2].x, leftVertices[i][6].y, leftVertices[i][6].z);
                leftVertices[i][12] = new Vector3(i == 0 ? prevVert[prevSize - 1][2].x : leftVertices[i - 1][2].x, leftVertices[i][6].y, leftVertices[i][6].z);
                leftVertices[i][20] = new Vector3(i == 0 ? prevVert[prevSize - 1][2].x : leftVertices[i - 1][2].x, leftVertices[i][6].y, leftVertices[i][6].z);
            }
            float x = 5 - roadWidth / 2 + addvalue;
            leftVertices[i][2] = new Vector3(x>0?x :0, leftVertices[i][2].y, leftVertices[i][2].z);
            leftVertices[i][8] = new Vector3(x > 0 ? x : 0, leftVertices[i][2].y, leftVertices[i][2].z);
            leftVertices[i][22] = new Vector3(x > 0 ? x : 0, leftVertices[i][2].y, leftVertices[i][2].z);

            leftVertices[i][4] = new Vector3(x > 0 ? x : 0, leftVertices[i][4].y, leftVertices[i][4].z);
            leftVertices[i][10] = new Vector3(x > 0 ? x : 0, leftVertices[i][4].y, leftVertices[i][4].z);
            leftVertices[i][21] = new Vector3(x > 0 ? x : 0, leftVertices[i][4].y, leftVertices[i][4].z);

            leftMesh[i].vertices = leftVertices[i];
            leftMesh[i].RecalculateBounds();
            leftMeshCollider[i].sharedMesh = leftMesh[i];

            leftwall[i].GetComponent<TreePositioner>().updatePosition();
        }
        if (transform.parent.GetComponent<FloorController>().isRelative)
        {
            /*rigtwall Relative movment*/
            for (int i = 0; i < rightVertices.Count; i++)
            {
                if (prevSize > 0)
                {
                    rightVertices[i][1] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][1].y, rightVertices[i][1].z);
                    rightVertices[i][13] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][1].y, rightVertices[i][1].z);
                    rightVertices[i][16] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][1].y, rightVertices[i][1].z);

                    rightVertices[i][7] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                    rightVertices[i][15] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                    rightVertices[i][19] = new Vector3(i == 0 ? prevVertRight[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                }

                float x3 = -10 + leftVertices[i][2].x + roadWidth;
                rightVertices[i][3] = new Vector3(x3 < 0 ? x3 : 0, leftVertices[i][2].y, leftVertices[i][2].z);
                rightVertices[i][9] = new Vector3(x3 < 0 ? x3 : 0, leftVertices[i][2].y, leftVertices[i][2].z);
                rightVertices[i][17] = new Vector3(x3 < 0 ? x3 : 0, leftVertices[i][2].y, leftVertices[i][2].z);

                float x4 = -10 + leftVertices[i][4].x + roadWidth;
                rightVertices[i][5] = new Vector3(x4 < 0 ? x4 : 0, leftVertices[i][4].y, leftVertices[i][4].z);
                rightVertices[i][11] = new Vector3(x4 < 0 ? x4 : 0, leftVertices[i][4].y, leftVertices[i][4].z);
                rightVertices[i][18] = new Vector3(x4 < 0 ? x4 : 0, leftVertices[i][4].y, leftVertices[i][4].z);

                rightMesh[i].vertices = rightVertices[i];
                rightMesh[i].RecalculateBounds();
                rightMeshCollider[i].sharedMesh = rightMesh[i];

                rightwall[i].GetComponent<TreePositioner>().updatePosition();
            }
        }
        else
        {
            /*rigtwall random movment*/
            List<Vector3[]> prevVertRigth = previewObjRef.GetComponent<FloorAtributes>().getRightVert();
            int prevRigthSize = prevVertRigth.Count;
            for (int i = 0; i < rightVertices.Count; i++)
            {
                float addvalue = Random.Range(-.1f, .3f) * randomFloorFactor;
                if (prevSize > 0)
                {
                    rightVertices[i][1] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][0].y, rightVertices[i][0].z);
                    rightVertices[i][14] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][0].y, rightVertices[i][0].z);
                    rightVertices[i][16] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][5].x : rightVertices[i - 1][5].x, rightVertices[i][0].y, rightVertices[i][0].z);

                    rightVertices[i][7] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                    rightVertices[i][15] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                    rightVertices[i][19] = new Vector3(i == 0 ? prevVertRigth[prevSize - 1][3].x : rightVertices[i - 1][3].x, rightVertices[i][7].y, rightVertices[i][7].z);
                }
                rightVertices[i][3] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][3].z);
                rightVertices[i][9] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][3].z);
                rightVertices[i][17] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][3].z);

                rightVertices[i][5] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][5].z);
                rightVertices[i][11] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][5].z);
                rightVertices[i][18] = new Vector3(-1.5f - addvalue, rightVertices[i][3].y, rightVertices[i][5].z);

                rightMesh[i].vertices = rightVertices[i];
                rightMesh[i].RecalculateBounds();
                rightMeshCollider[i].sharedMesh = rightMesh[i];
            }
        }
    }
}

