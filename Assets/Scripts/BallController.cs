﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class BallController : MonoBehaviour
{
    Rigidbody rb;
    public float growRate;
    public TextMeshPro sizeUi;
    public float speed;
    public FloorController floorController;
    public bool hasLunched = false;
    public bool hasArrivedTop = false;
    public bool hasArrived = false;
    public bool hasStopped = false;
    float positionAdvanced;
    int lives = 6;
    public Material greenMaterial;
    public Material orangeMaterial;
    public Material redMaterial;
    Vector3 movment;
    /*Initial Conditions*/
    Vector3 initialPosition;
    Vector3 initialScale;
    int initialLives;
    int trys = 0;
    public TextMeshPro trysUi;
    public Renderer renderer;
    public int direction;

	Gyroscope m_Gyro;
    
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.angularDrag = 0;
        rb.angularVelocity = new Vector3(0, 100, 0);
        rb.constraints = RigidbodyConstraints.FreezePositionZ;

        initialPosition = transform.position;
        initialLives = lives;
        initialScale = transform.localScale;

		m_Gyro = Input.gyro;
        m_Gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case 0:
                movment = new Vector2(0, 0);
                break;
            case 1:
                movment = new Vector2(1, 0);
                break;
            case 2:
                movment = new Vector2(-1, 0);
                break;
            default:
                break;
        }

        //if (Input.GetKeyDown("space"))
        //    ChangeDirection();
        //movment = new Vector2(Input.GetAxis("Horizontal"), 0);
        //if (Input.GetAxis("Vertical") != 0)
        //    launch();
        if (!hasLunched)
        {
            transform.localScale += Vector3.one * growRate * Time.deltaTime * 0.001f;
            rb.mass = transform.localScale.magnitude;
        }
        if (!hasStopped && ((hasLunched && rb.angularVelocity.magnitude < 0.15) || (positionAdvanced > transform.position.z)))
        {
            hasStopped = true;
            print("Has Stopped: " + hasStopped);
            rb.velocity = Vector3.zero;
            StartCoroutine(ReloadCoroutune());
        }
        positionAdvanced = transform.position.z;
    }

    public void ChangeDirection()
    {
        if (direction == 0 || direction == 1)
            direction = 2;
        else if (direction == 2)
            direction = 1;
    }

    IEnumerator ReloadCoroutune()
    {
        yield return new WaitForSeconds(2);
        ReloadGame();
    }

    void LateUpdate()
    {
        sizeUi.SetText(gameObject.transform.localScale.magnitude.ToString("F2"));
    }

    void FixedUpdate()
    {
        MovePosition();
        if (m_Gyro.attitude.x > 0)
            direction = 1;
        else
            direction = 2;
        if (m_Gyro.rotationRate.x > 10)
            launch();
    }

    public void launch()
    {
        if (hasLunched)
            return;
        hasLunched = true;
        rb.velocity = Vector3.up * 40;
        StartCoroutine(moveToBuildings());
    }

    IEnumerator moveToBuildings()
    {
        yield return new WaitForSeconds(2);
        hasArrivedTop = true;
        yield return new WaitForSeconds(2);

        if (!hasArrived)
        {
            hasArrived = true;
            transform.position = Vector3.forward * 40 + transform.position;// new Vector3(0, 9f, 40);
            rb.constraints = RigidbodyConstraints.None;
            //rb.velocity = Vector3.forward * floorController.speed * floorController.speed;
            //rb.velocity = Vector3.forward * floorController.speed * 20;
            rb.velocity = Vector3.forward * floorController.speed;
            rb.angularDrag = .5f;
        }
    }

    void MovePosition()
    {
        if (!hasStopped && movment.magnitude > 0)
        {
            //rb.AddForce(Vector3.right * Input.GetAxis("Horizontal") * speed * 5);
            //rb.MovePosition(transform.position + (movment * speed * Time.deltaTime));
            //rb.velocity = new Vector3(Input.GetAxis("Horizontal") * speed / rb.mass, rb.velocity.y, rb.velocity.z);
            rb.velocity = new Vector3(movment.x * speed / rb.mass, rb.velocity.y, rb.velocity.z);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if ("Respawn".Equals(collision.collider.gameObject.tag))// && collision.relativeVelocity.magnitude>0)
        {
            lives--;
            updateColor(lives);
            if (lives <= 0)
            {
                Debug.Log("Game Over");
                ReloadGame();
            }
        }
    }

    private void updateColor(int lives)
    {
        switch (lives)
        {
            case 6:
            case 5:
                renderer.material = greenMaterial;
                break;
            case 3:
            case 4:
                renderer.material = orangeMaterial;
                break;
            case 2:
            case 1:
                renderer.material = redMaterial;
                break;
        }
    }

    private void ReloadGame()
    {
        direction = 0;
        rb.velocity = Vector3.zero;
        hasStopped = false;
        hasLunched = false;
        hasArrived = false;
        hasArrivedTop = false;
        transform.position = initialPosition;
        transform.localScale = initialScale;
        positionAdvanced = transform.position.z-1;
        floorController.ReloadGame();
        lives = initialLives;
        updateColor(lives);
        Start();
        trys++;
        trysUi.SetText(trys.ToString());
    }
}
