﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class FloorController : MonoBehaviour
{
    public List <GameObject> floors = new List<GameObject>();
    public GameObject floorsBucket;
    public float speed;
    public float randomFactor;
    public bool isRelative;
    public float roadWidth;
    public float acelerationRate;
    public float roadWitdhChangeRate;
    public TextMeshPro speedUi;
    public BallController ballController;
    private int renewTimes=0;

    /*Initial Conditions*/
    float initialSpeed;
    float initialRoadWidth;

    void Start()
    {
        initialSpeed = speed;
        initialRoadWidth = roadWidth;

        /*Firstone*/
        floors.Add(
                GameObject.Instantiate((floorsBucket),
                new Vector3(0,0,floors.Count*floorsBucket.transform.localScale.x),
                Quaternion.Euler(90,0,0),
                transform));
        /*midle ones*/
        for (int i=1; i< 4; i++){
            floors.Add(
                GameObject.Instantiate((floorsBucket),
                new Vector3(0,0,floors.Count*floorsBucket.transform.localScale.x),
                Quaternion.Euler(90,0,0),
                transform));
            floors[i].GetComponent<FloorAtributes>().setPrevObjRef(floors[i-1]);
            if (i == 2)
            {
                floors[i].transform.Find("Text").gameObject.SetActive(true);
            }
        }
        /*lastone reference*/
        floors[0].GetComponent<FloorAtributes>().setPrevObjRef(
                floors[floors.Count-1]);
    }

   
    void Update()
    {
        speed += acelerationRate * Time.deltaTime;
        roadWidth -= roadWitdhChangeRate * Time.deltaTime;
        foreach (GameObject floor in floors)
        {
            floor.GetComponent<Rigidbody>().velocity = (Vector3.back * speed);
            //floor.GetComponent<Rigidbody>().MovePosition(Vector3.down * speed * Time.deltaTime);
            //floor.transform.Translate(Vector3.down * speed * Time.deltaTime);
            if (floor.transform.position.z < -5 ){
                floor.transform.position = floor.transform.position + Vector3.forward * floors.Count * floorsBucket.transform.localScale.x;
                floor.GetComponent<FloorAtributes>().Renew();

                renewTimes++;
                if (ballController.direction == 0 && renewTimes>2){
                    ballController.direction = 1;
                }
            }
        }
        speedUi.SetText((speed).ToString("F2"));
    }

    internal void ReloadGame()
    {
        speed = initialSpeed;
        roadWidth = initialRoadWidth;
        foreach (GameObject obj in floors){
            GameObject.Destroy(obj);
        }
        floors.Clear();
        Start();
    }
}
