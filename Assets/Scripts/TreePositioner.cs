﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreePositioner : MonoBehaviour
{

    public GameObject tree;

    public void updatePosition()
    {
        if (Random.Range(0.0f, 1.0f) > 0.5f)
        {
            tree.SetActive(true);
            Vector3[] vertices = GetComponent<MeshFilter>().mesh.vertices;
            float x = (vertices[0].x + vertices[1].x + vertices[2].x + vertices[3].x) / 4;
            float y = (vertices[0].y + vertices[1].y + vertices[2].y + vertices[3].y) / 4;
            tree.transform.localPosition = new Vector3(x, y, -1);
        }
        else
        {
            tree.SetActive(false);
        }
    }

}
